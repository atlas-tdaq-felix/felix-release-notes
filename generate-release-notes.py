#!/usr/bin/env python3
"""
generate-release-notes - Generates release notes.

usage: generate-release-notes [options] <release>...

options:
  -v, --version         Display version.
  -h, --help            Prints a short usage summary.
  -n, --notepath=<path> Path to notes

arguments:
  <release>...            Releases as named in Jira, felix-04-02-00 felix-drivers-04-06-00
"""

import fnmatch
import os
import sys

from collections import defaultdict

from docopt import docopt  # noqa: E402

from jira import JIRA, JIRAError  # noqa: E402
import codecs

issuetype_table = {
    "Question": "Q",
    "New Feature": "N",
    "Task": "T",
    "Bug": "B",
    "Improvement": "I"
}

html_escape_table = {
    "&": "&amp;",
    '"': "&quot;",
    "'": "&apos;",
    ">": "&gt;",
    "<": "&lt;",
}


def html_escape(text):
    """Produce entities within text."""
    return "".join(html_escape_table.get(c, c) for c in text)


if __name__ == "__main__":

    args = docopt(__doc__, version="4.2")
    releases = args['<release>']
    notes = args['--notepath']

    flx_token = os.environ.get('FLX_TOKEN')
    if not flx_token:
        print("Error: no FLX_TOKEN supplied")
        sys.exit(1)

    project = 'FLX'
    jira = None
    try:
        jira = JIRA('https://its.cern.ch/jira', token_auth=flx_token)
    except JIRAError as e:
        print(e.status_code, e.text)
        sys.exit(1)
        if e.status_code == 401:
            print("Login to JIRA failed. Check your token")
            sys.exit(1)

    # Make sure demanded releases exist in JIRA (or partially)
    versions = jira.project_versions(project)
    query = []
    for release in releases:
        for version in versions:
            if release.startswith(str(version)):
                query.append(str(version))
                break

    query = ",".join(query)

    issues = jira.search_issues('project=' + project + ' AND fixVersion in (' + query + ')', maxResults=False)
    # print(issues.total)

    data = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict())))

    for i in issues:
        issue = jira.issue(i)

        for fixVersion in issue.fields.fixVersions:
            for component in issue.fields.components:
                data[fixVersion.name][component.name][issue.fields.issuetype.name][issue.key] = (issue.fields.status.name, issue.fields.summary)
            if len(issue.fields.components) == 0:
                data[fixVersion.name]["other"][issue.fields.issuetype.name][issue.key] = (issue.fields.status.name, issue.fields.summary)

    projectName = jira.project(project).name
    print(''.join(('<h1>Release Notes - ', projectName, '</h1>')))
    print('<h2>Table of Contents</h2>')
    print('<nav>')
    print('<ul>')
    for release in releases:
        for version in sorted(data.keys()):
            if fnmatch.fnmatch(version, release):
                print('<li><a href="#' + version.strip() + '">' + version + '</a></li>')
    print('</ul>')
    print('</nav>')
    print('<hr>')
    print('Key: <b>B</b>: Bug, <b>I</b>: Improvement, <b>N</b>: New Feature, <b>Q</b>: Question, <b>T</b>: Task')
    for release in releases:
        for version in sorted(data.keys()):
            if fnmatch.fnmatch(version, release):
                print('<hr>')
                print(''.join(('<h2 id="', version.strip(), '">Version ', version, '</h2>')))
                print('Key changes: <br />')
                try:
                    inputHTML = codecs.open(notes + "/" + version + ".html", 'r')
                    print(inputHTML.read())
                except FileNotFoundError:
                    print('No information available <br />')
                print('Full list of changes below:')
                for component in sorted(data[version].keys()):
                    print(''.join(('<h3>', component, '</h3>')))
                    for issuetype in sorted(data[version][component].keys()):
                        itype = issuetype_table.get(issuetype, issuetype)
                        print('<ul>')
                        for key in sorted(data[version][component][issuetype].keys()):
                            status = 'disc' if data[version][component][issuetype][key][0] in ('Resolved', 'Closed') else 'circle'
                            summary = html_escape(data[version][component][issuetype][key][1])
                            print(''.join(('<li style="list-style-type:', status, ';">', '<a href="https://its.cern.ch/jira/browse/', key, '">', key, '</a> [<b>', itype, '</b>] - ', summary, '</li>')))
                        print('</ul>')
